const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const development = !!process.env.DEV;

const paths = {
  src: {
    root: path.resolve(__dirname, 'src'),
  },
  dist: {
    root: path.resolve(__dirname, 'dist'),
  }
};

const entries = {
  index: paths.src.root,
};

const config = {
  context: path.resolve(__dirname, 'src'),
  entry: entries,
  output: {
    path: paths.dist.root,
    filename: 'assets/[name].js',
    publicPath: '/',
  },
  mode: !development ? 'development' : 'production',
  resolve: {
    alias: {
      engine: path.resolve(__dirname, 'src', 'engine'),
      game: path.resolve(__dirname, 'src', 'game'),
      assets: path.resolve(__dirname, 'src', 'assets'),
    },
    modules: ['node_modules'],
    descriptionFiles: ['package.json'],
    extensions: ['.js', '.ts', '.json'],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: './index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'assets/[name].css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.ts$/,
        use: 'ts-loader',
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
      },
      {
        test: /\.p?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ]
      },
    ]
  }
};

if (development) {
  config.devtool = 'source-map';
  config.devServer = {
    host: '0.0.0.0',
    port: 8080,
  }
}

module.exports = config;
