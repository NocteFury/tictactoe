import 'assets/index.css';

import { combineLatest } from 'rxjs';

import { Engine } from 'engine';
import { TicTacToe } from 'game';
import { Tile } from 'game/logic';

// Базовые внешние настроки приложения
const options = { width: 1000, height: 1000 };

// Контейнер и панель приложения
const container = document.getElementById('viewport');
const info = document.getElementById('info');

const form = document.forms['setup'];

// Экземпляр игрового движка и игрового цикла
const engine = new Engine<TicTacToe>(container, TicTacToe, options).launch();
const game = engine.gameloop;

const launcher = createLauncher();

engine.view.className = 'viewport';

form.addEventListener('submit', args => {
  args.preventDefault();

  const data = new FormData(form);

  const size = +data.get('size');
  const toWin = +data.get('win');
  const first = data.get('first');

  launcher(size, toWin, first as Tile);
});

launcher(3, 3);

// ------------------------------------------------------------------------

function createLauncher() {
  let subscription;

  return (size: number, toWin: number, first?: Tile) => {
    if (subscription) subscription.unsubscribe();

    // Запуск раунда игры
    const round = game.start(size, toWin, first);

    // Обновление статуса игры
    subscription = combineLatest([
      round.winner$,
      round.player$,
    ]).subscribe(([winner, player]) => {
      // Если есть победитель, то показываем кто
      if (winner) info.textContent = `Победитель: ${winner.tile}`;
      // Иначе показываем, чей ход
      else info.textContent = `Ходит: ${player.tile}`;
    });
  }
}
