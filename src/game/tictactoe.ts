import { Application } from 'pixi.js';

import { GameLoop } from 'engine';

import { Round, Board, Tile } from 'game/logic';
import { World } from 'game/view';

export class TicTacToe extends GameLoop {
  private round: Round; // Текущий раунд игры
  private world: World; // Игровое пространство

  // Сохраняем ссылку на приложение PIXI.js
  constructor(private app: Application) { super(); }

  /**
   * Начало нового рануда
   * @param size - размер игрового поля (длина и ширина)
   * @param toWin - количество элементов подряд для победы
   * @param first - какой символ будет ходить первым
   */
  start(size: number, toWin = 3, first: string) {
    if (size < 3) return; // Если количество элементов для победы меньше 3 - не создавать раунд

    // Очищаем игровое пространство
    this.dispose();

    // Создается новый раунд и игровое пространство
    this.round = new Round(new Board(size), toWin, first as Tile);
    this.world = new World(size);

    // Добавляем игровое пространство на сцену
    this.app.stage.addChild(this.world);

    // В случае, если в игровом пространстве игрок выбрал клетку, то...
    this.world.on('choose', args => {
      // Определяем, чей сейчас ход
      const current = this.round.getCurrent();

      // Пробуем походить текущим игроком по указанной клетке
      // Если она занята, то ничего не делаем
      if (this.round.turn(args.position.x, args.position.y))
        // Устанавлвиваем значок текущего игрока в указанную клетку
        this.world.setTile(args.position.x, args.position.y, current.tile);

      // Если игра звершена (если предыдущий ход повлек за собой победу), то...
      if (this.round.isFinished()) {
        // Рисуем победную линию
        this.world.drawWinLine(this.round.getWinLine());
      }
    });

    // Изменяем размер окна приложения в соответствии с размерами игрового полотна
    this.app.renderer.resize(this.world.getWidth(), this.world.getHeight());

    // Возвращаем текущий раунд
    return this.round;
  }

  // При обновлении приложения, обновляем игровое пространство, если таковое имеется
  onUpdate(delta: number) {
    this.world && this.world.update(delta);
  }

  onDestroy() {
    // Очиащем игровое пространство
    this.dispose();
  }

  private dispose() {
    // Если ранее было создано игровое пространство - удалить
    if (this.world) {
      this.app.stage.removeChild(this.world);
      this.world.destroy();
    }
  }
}
