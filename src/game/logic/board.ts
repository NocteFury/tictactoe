import { Point } from 'pixi.js';

// Возможные символы на игровом поле
export enum Tile { None = 'none', Cross = 'cross', Circle = 'circle' }

// Класс доски отвечает за обработку логической части игрового пространства
export class Board {
  // Массив игровых клеток
  private readonly field: Tile[][];

  // Создаем массивы игровых клеток в соответствии с указанными размерами
  constructor(private size: number) {
    this.field = Array.from({ length: size }).map(() => []);
  }

  /**
   * Устанавливает указанный символ по указанным координатам
   * Установить символ можно даже на уже занятый.
   * Такой проверки в этом методе нет и быть не должно
   */
  setTile(x: number, y: number, tile: Tile) {
    if (x < 0 || y < 0 || x >= this.size || y >= this.size) return;
    this.field[x][y] = tile;
  }

  /**
   * Возвращает символ по указанным координатам
   * Если такого нет, то возвращает Tile.None
   */
  getTile(x: number, y: number) {
    if (x < 0 || y < 0 || x >= this.size || y >= this.size) return null;
    return this.field[x][y] || Tile.None;
  }

  /**
   * Проверяет, победил ли указанный игрок, в момент установки символа по указанным координатам
   */
  check(x: number, y: number, tile: Tile, toWin: number): boolean {
    return !!this.getResult(x, y, tile, toWin);
  }

  /**
   * Возвращает массив координат с первым попавшимся вариантом, в которых игрок победил
   * Если ни одного варианта нет - возвращает null
   */
  getResult(x: number, y: number, tile: Tile, toWin: number): Point[] {
    const dia0 = this.getLength(x, y, 1, 1, tile);
    if (dia0.length >= toWin) return dia0;

    const dia1 = this.getLength(x, y, -1, 1, tile);
    if (dia1.length >= toWin) return dia1;

    const horizontal = this.getLength(x, y, 1, 0, tile);
    if (horizontal.length >= toWin) return horizontal;

    const vertical = this.getLength(x, y, 0, 1, tile);
    if (vertical.length >= toWin) return vertical;

    return null
  }

  /**
   * Возвращает массив подряд идущих символов в указанном направлении
   * Проверка производится в обе стороны (т.е. при указании dx = 0 и dy = 1)
   * то проверка будет произведена как вверх, так и вниз.
   */
  private getLength(x: number, y: number, dx: number, dy: number, tile: Tile): Point[] {
    let result = [new Point(x, y)];
    let cursor;

    cursor = new Point(x + dx, y + dy);
    while (this.getTile(cursor.x, cursor.y) === tile) {
      result.push(cursor.clone());
      cursor.set(cursor.x + dx, cursor.y + dy);
    }

    cursor = new Point(x - dx, y - dy);
    while (this.getTile(cursor.x, cursor.y) === tile) {
      result.push(cursor.clone());
      cursor.set(cursor.x - dx, cursor.y - dy);
    }

    return result;
  }
}
