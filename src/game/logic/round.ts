import { Point } from 'pixi.js';
import { BehaviorSubject, Observable } from 'rxjs';

import { Board, Player, Tile } from 'game/logic';

// Раунд игры
export class Round {
  // Хранит порядковый номер текущего игрока
  private current: number = 0;

  // Хранит список всех активных в раунде игроков
  private readonly players: Player[];

  // Закончена игра или нет
  private gameover = false;

  // Победная линия, повлекшая за собой победу игрока
  private winLine: Point[];

  private player: BehaviorSubject<Player>; // Поток текущего игрока
  private winner: BehaviorSubject<Player>; // Поток победителя

  // Возвращает потоки, как Observable
  get player$(): Observable<Player> { return this.player.asObservable(); }
  get winner$(): Observable<Player> { return this.winner.asObservable(); }

  /**
   * Конструктор создания раунда
   * @param board - игровое поле
   * @param toWin - необходимое количество символов подря для победы
   * @param first - символ, который идет первым
   */
  constructor(private board: Board, readonly toWin = 3, readonly first = Tile.Cross) {
    // Создает массив игроков
    // (В будущем, можно их передавать в конструктор, в случае многопользовательской игры по сети)
    this.players = [new Player(first), new Player(this.getOpponent(first))];

    // Инициализация потоков
    this.player = new BehaviorSubject(this.getCurrent());
    this.winner = new BehaviorSubject(null);
  }

  // Возвращает текущего игрока. Возвращает null, если игра окончена
  getCurrent(): Player {
    return this.gameover ? null : this.players[this.current];
  }

  // Возвращает победную последовательность
  getWinLine(): Point[] {
    return this.winLine;
  }

  // Возвращает, закончена игра или нет
  isFinished(): boolean {
    return this.gameover;
  }

  // Производит поптыку "походить" текущим игроком в указанной клетке
  turn(x: number, y: number): boolean {
    // Если в указанной клетке уже есть символ или игра окончена, возвращать false
    if (this.board.getTile(x, y) !== Tile.None || this.gameover) return false;

    // Возвращает текущего игрока (при этом значение счетчика меняется автоматически)
    const current = this.players[this.current++];

    // Если текущего игрока нет, то счетчик обнулить
    if (this.current >= this.players.length) this.current = 0;

    // Проверям, победил ли игрок
    if (this.board.check(x, y, current.tile, this.toWin)) {
      // Сохраняем благодаря какой последовательности победил игрок
      this.winLine = this.board.getResult(x, y, current.tile, this.toWin);

      // Помечаем, что игра окончена
      this.gameover = true;

      // Устанавливаем результаты
      this.winner.next(current);
      this.player.next(null);
    } else {
      // ...иначе указываем, что сейчас ход другого игрока
      this.player.next(this.getCurrent());
    }

    // Сохраняет текущую клетку на доске
    this.board.setTile(x, y, current.tile);

    return true;
  }

  // Возвращает опонента символа
  private getOpponent(tile: Tile): Tile {
    switch (tile) {
      case Tile.Circle: return Tile.Cross;
      case Tile.Cross: return Tile.Circle;
      default: return Tile.None;
    }
  }
}
