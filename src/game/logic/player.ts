import { Tile } from './board';

// Класс-обертка игрока
// Хранит символ, которым управляет игрок
export class Player {
  constructor(readonly tile: Tile) { }
}
