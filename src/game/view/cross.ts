import { Container, Graphics, Point } from 'pixi.js';

const speed = 0.1;

export class Cross extends Container {
  private shape = new Graphics();

  private progress: number = 0;

  constructor(private size: number) {
    super();

    this.addChild(this.shape);
  }

  animate(delta: number) {
    if (this.progress < 1) {
      this.progress = Math.min(this.progress + speed * delta, 1);

      this.shape.clear();
      this.shape.lineStyle(5, 0x0000FF);

      const from1 = new Point(this.size / 2 - this.size / 4, this.size / 2 - this.size / 4);
      const to1 = this.interpolate(
        from1,
        new Point(this.size / 2 + this.size / 4, this.size / 2 + this.size / 4),
        this.progress,
      );

      this.shape.moveTo(from1.x, from1.y);
      this.shape.lineTo(to1.x, to1.y);

      const from2 = new Point(this.size / 2 + this.size / 4, this.size / 2 - this.size / 4);
      const to2 = this.interpolate(
        from2,
        new Point(this.size / 2 - this.size / 4, this.size / 2 + this.size / 4),
        this.progress,
      );

      this.shape.moveTo(from2.x, from2.y);
      this.shape.lineTo(to2.x, to2.y);
    }
  }

  private interpolate(from: Point, to: Point, progress: number): Point {
    const diff = new Point(to.x - from.x, to.y - from.y);
    return new Point(from.x + diff.x * progress, from.y + diff.y * progress);
  }
}
