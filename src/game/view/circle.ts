import { Container, Graphics } from 'pixi.js';

const speed = 0.3;

export class Circle extends Container {
  private shape = new Graphics();

  private progress: number = 0;

  constructor(private size: number) {
    super();

    this.addChild(this.shape);
  }

  animate(delta: number) {
    if (this.progress < Math.PI * 2) {
      this.progress = Math.min(this.progress + speed * delta, Math.PI * 2);

      this.shape.clear();
      this.shape.lineStyle(5, 0xFF0000);
      this.shape.arc(this.size / 2, this.size / 2, this.size / 4, 0, this.progress);
    } else {
      this.shape.clear();
      this.shape.lineStyle(5, 0xFF0000);
      this.shape.drawCircle(this.size / 2, this.size / 2, this.size / 4);
    }
  }
}
