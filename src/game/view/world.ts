import { Container, Sprite, Texture, Graphics, Point } from 'pixi.js';

import { Tile } from 'game/logic';
import { Circle, Cross } from 'game/view';

const scale = 80;

const color = {
  even: 0xFFFFFF,
  odd: 0xDDDDDD,
};

// Игровое поле
export class World extends Container {
  // Размер игрового поля
  private readonly size: number;

  // Игровое поле (массив спрайтов)
  private readonly tiles: Array<Circle | Cross> = [];

  constructor(size: number) {
    super();

    this.size = size;

    // Создает шахматную сетку
    const tiles = Array.from({ length: size * size }).map((_, i: number) => this.createTile(i));
    this.addChild(...tiles);
  }

  // Возвращает ширину игрового поля (в пикселях)
  getWidth() {
    return this.size * scale;
  }

  // Возвращает высоту игрового поля (в пикселях)
  getHeight() {
    return this.size * scale;
  }

  // Устанавливает символ по координатам
  setTile(x: number, y: number, tile: Tile) {
    if (x < 0 || y < 0 || x >= this.size || y >= this.size) return;

    const sprite = tile === Tile.Circle ? new Circle(scale) : new Cross(scale);
    sprite.position.set(x * scale, y * scale);
    this.addChild(sprite);

    this.tiles.push(sprite);
  }

  // Отрисовывает линию победы
  drawWinLine(tiles: Point[]) {
    const line = new Graphics();
    line.lineStyle(2, 0x00FFFF);
    tiles.forEach((position, i) => {
      if (i === 0) line.moveTo(position.x * scale + scale / 2, position.y * scale + scale / 2);
      else line.lineTo(position.x * scale + scale / 2, position.y * scale + scale / 2);
    });
    this.addChild(line);
  }

  // Обновляет анимацию символов
  update(delta: number) {
    this.tiles.forEach(tile => tile.animate(delta));
  }

  // Создает символ
  private createTile(i: number) {
    const tile = new Sprite(Texture.WHITE);
    const position = this.toPosition(i);

    tile.position.set(position.x * scale, position.y * scale);
    tile.width = tile.height = scale;

    tile.tint = this.getTileColor(i);

    tile.interactive = true;
    tile.buttonMode = true;

    // В случае клике по символу эмитит событие 'choose'
    tile.on('pointerup', args => this.emit('choose', { ...args, position: this.toPosition(i) }));
    return tile;
  }

  // Возвращает цвет клетки, в зависимости от индекса
  private getTileColor(i: number) {
    if (this.size % 2) return i % 2 ? color.even : color.odd;

    if (Math.floor(i / this.size) % 2) return i % 2 ? color.odd : color.even;
    else return i % 2 ? color.even : color.odd;
  }

  // Возвращает координаты от индекса
  private toPosition(i: number) {
    return new Point(i % this.size, Math.ceil((i + 1) / this.size) - 1);
  }
}
