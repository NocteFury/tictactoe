import { Application } from 'pixi.js';

import { GameLoop } from './gameloop';

const defaults = {
  autoStart: false,
  transparent: false,
  autoDensity: true,
  antialias: true,
  backgroundColor: 0xFFFFFF,
};

export class Engine<T extends GameLoop> {
  // Основной игровой цикл
  readonly gameloop: T;

  // Ссылка на приложение PIXI.js
  private readonly app: Application;

  // Хранит состояние приложения. Запущено оно или нет.
  private running = false;

  /**
   * Конструктор создания приложения
   * @param container - HTML элемент, внутри которого необходимо создать canvas
   * @param gameloop - класс игрового цикла
   * @param options - опции настроек игрового приложения
   */
  constructor(container: HTMLElement, gameloop: any, options: any = {}) {
    this.app = new Application({ ...defaults, ...options }); // Создаем приложение
    this.gameloop = new gameloop(this.app); // Создаем экземпляр класса игрового цикла

    container.append(this.app.view); // Добавляем в контейнер canvas игры
  }

  get view() {
    return this.app.view;
  }

  // Запуск игрового приложения
  launch() {
    // Запуск иницализации
    this.gameloop.onInit();

    // Запуск приложения PIXI.js
    this.app.start();

    // Обновление приложение на каждый тик. Передача разницы между кадрами в мс
    this.app.ticker.add(() => this.gameloop.onUpdate(this.app.ticker.deltaTime));

    this.running = true; // Помечаем приложение как активное

    return this;
  }

  // Остановка и очистка приложения
  shutdown() {
    this.app.stop();
    this.gameloop.onDestroy();

    this.running = false; // Помечаем приложение как не активное
  }
}
