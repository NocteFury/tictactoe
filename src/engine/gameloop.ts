export class GameLoop {
  onInit() {}
  onUpdate(delta: number) {}
  onDestroy() {}
}
